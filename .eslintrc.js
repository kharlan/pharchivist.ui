/* eslint-env node */
module.exports = {
	env: {
		browser: true,
		es2021: true
	},
	extends: [
		'wikimedia',
		'wikimedia/client-common',
		'wikimedia/language/es2020'
	],
	overrides: [
		{
			files: [ '**/*.vue' ],
			parser: 'vue-eslint-parser',
			extends: [
				'wikimedia/vue3-common',
				'wikimedia/client-common'
			],
			rules: {
				'vue/no-unsupported-features': [ 'error', {
					version: '^3.2.27'
				} ],
				'vue/component-name-in-template-casing': [ 'error', 'kebab-case' ],
				'vue/custom-event-name-casing': [ 'error', 'kebab-case' ],
				'vue/no-undef-components': [ 'error', {
					ignorePatterns: [ 'router(\\-\\w+)+', 'ais(\\-\\w+)+' ]
				} ]
			}
		}
	],
	parserOptions: {
		ecmaVersion: 'latest',
		sourceType: 'module'
	},
	plugins: [
		'vue'
	],
	rules: {}
};
