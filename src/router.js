import { createRouter, createWebHistory } from 'vue-router';
import HomeView from './views/Home.vue';
import TaskView from './views/Task.vue';

export const router = createRouter( {
	history: createWebHistory(),
	routes: [
		{
			path: '/',
			component: HomeView,
			name: 'Home'
		},
		{
			path: '/tasks/:taskId',
			component: TaskView,
			name: 'Tasks'
		}
	]
} );
