import { MeiliSearch } from 'meilisearch';

async function getPost( taskId ) {
	const client = new MeiliSearch( {
		host: 'https://meilisearch.kostaharlan.net',
		apiKey: '93121c6eee137e30a805eee7bd6e60f3422748ff696fd75324eac0580b7ee6a6'
	} );
	const tasks = await client.getIndex( 'tasks' );
	return await tasks.getDocument( taskId );
}

export default getPost;
